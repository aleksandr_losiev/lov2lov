import Vue from 'vue'
import Vuex from 'vuex'

import main from './store/main'
import categories from './store/categories'
import products from './store/products'
import types from './store/types'
import friends from './store/friends'
import socials from './store/socials'
import colors from './store/colors'
import sizes from './store/sizes'
import sorting from './store/sorting'
import attributes from './store/attributes'
import cart from './store/cart'

Vue.use(Vuex);

export default new Vuex.Store({
    namespaced: true,
    modules: {
      main,
      categories,
      products,
      types,
      friends,
      socials,
      colors,
      sizes,
      sorting,
      attributes,
      cart
    }
})
