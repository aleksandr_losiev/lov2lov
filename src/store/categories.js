const state = {
    list: [
        {id: 0, key: 'all', name: 'All', customClass: ''},
        {id: 1, key: 'dresses', name: 'Платья', customClass: ''},
        {id: 2, key: 'suits', name: 'Костюмы', customClass: ''},
        {id: 3, key: 'skirts', name: 'Юбки', customClass: ''},
        {id: 4, key: 'blouses', name: 'Блузки', customClass: ''},
        {id: 5, key: 'trousers', name: 'Брюки', customClass: ''},
        {id: 6, key: 'body', name: 'Боди', customClass: ''},
        {id: 7, key: 'tops', name: 'Топы', customClass: ''},
        {id: 8, key: 'outerwear', name: 'Верхняя одежда', customClass: ''},
        {id: 9, key: 'swimwear', name: 'Купальники', customClass: ''},
        {id: 10, key: 'sale', name: 'Sale', customClass: 'text__red text__bold'},
    ]
};

const getters = {
    getCategoriesByID(state) {
        return ids => state.list.filter( item => {
            return ids.includes(item.id);
        });
    },
    getCategoryByKey(state) {
        return key => state.list.find( item => item.key === key);
    }
};

export default {
    namespaced: true,
    state,
    getters
}