const state = {
    list: [
        {id: 0, name: 'Белый', className: 'bg-color__white'},
        {id: 1, name: 'Чёрный', className: 'bg-color__black'},
        {id: 2, name: 'Оливковый', className: 'bg-color__olive'},
        {id: 3, name: 'Красный', className: 'bg-color__red'},
        {id: 4, name: 'Светло-серый', className: 'bg-color__light-gray'},
        {id: 5, name: 'Розовый', className: 'bg-color__pink'},
        {id: 6, name: 'Персиковый', className: 'bg-color__peach'},
        {id: 7, name: 'Бирюзовый', className: 'bg-color__turquoise'},
        {id: 8, name: 'Синий', className: 'bg-color__blue'},
        {id: 9, name: 'Зелёный', className: 'bg-color__green'},
        {id: 10, name: 'Серый', className: 'bg-color__gray'},
    ]
};

const getters = {
    getColorByID(state) {
        return id => {
            return state.list.find(item => item.id === id);
        }
    },
    getColorsByID(state) {
        return ids => state.list.filter( item => {
            return ids.includes(item.id);
        });
    },
};

export default {
    namespaced: true,
    state,
    getters
}