const state = {
    list: [
        {id: 0, name: 'XS', type: {
            bust: '85-90',
            waist: '55-60',
            hips: '85-90'
        }},
        {id: 1, name: 'S', type: {
            bust: '90-95',
            waist: '60-65',
            hips: '90-95'
        }},
        {id: 2, name: 'M', type: {
            bust: '95-100',
            waist: '65-70',
            hips: '95-105'
        }},
        {id: 3, name: 'L', type: {
            bust: '100-110',
            waist: '70-80',
            hips: '105-115'
        }}
    ]
};

const getters = {
    getSizeByID(state) {
        return id => {
            return state.list.find(item => item.id === id);
        }
    },
};

export default {
    namespaced: true,
    state,
    getters
}