const state = {
    list: [
        {id: 0, icon: 'fa-facebook-square', link: 'https://uk-ua.facebook.com/'},
        {id: 1, icon: 'fa-instagram', link: 'https://www.instagram.com'},
    ]
};

export default {
    namespaced: true,
    state,
}