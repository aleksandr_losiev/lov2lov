const state = {
    currency: 'Р',
    popup: '',
    filters: [0, 1],
    menu: {
        isOpen: false
    },
    history: {
        isOpen: false,
        productsID: []
    },
    favorite: {
        isOpen: false,
        productsID: []
    },
    navigation: {
        isOpen: ''
    },
    isOpenFilter: false,
    isOpenSorting: false,
    closeModal: false,
};

const mutations = {
    setIsOpenFilter(state, status) {
        state.isOpenFilter = status;
    },
    setIsOpenSorting(state, status) {
        state.isOpenSorting = status;
    },
    setCloseModal(state, status) {
      state.closeModal = status;
    },
    setMenuStatus(state, status) {
        state.menu.isOpen = status;
    },
    setNavigationStatus(state, status) {
        state.navigation.isOpen = status;
    },
    setProductInHistory(state, list) {
        state.history.productsID = list;
    },
    setProductInFavorite(state, list) {
        state.favorite.productsID = list;
    },
    setHistoryStatus(state, status) {
        state.history.isOpen = status;
    },
    setFavoriteStatus(state, status) {
        state.favorite.isOpen = status;
    },
    setPopup(state, status) {
        state.popup = status;
    }
};

export default {
    namespaced: true,
    state,
    mutations
}