const state = {
    list: [
        {
            id: 0,
            title: 'Цвет',
            slug: 'colors',
            data: [
                {id: 0, attr_id: 0, name: 'Белый'},
                {id: 1, attr_id: 0, name: 'Чёрный'},
                {id: 2, attr_id: 0, name: 'Оливковый'},
                {id: 3, attr_id: 0, name: 'Красный'},
                {id: 4, attr_id: 0, name: 'Светло-серый'},
                {id: 5, attr_id: 0, name: 'Розовый'},
                {id: 6, attr_id: 0, name: 'Персиковый'},
                {id: 7, attr_id: 0, name: 'Бирюзовый'},
                {id: 8, attr_id: 0, name: 'Синий'},
                {id: 9, attr_id: 0, name: 'Зелёный'},
                {id: 10, attr_id: 0, name: 'Серый'},
            ],
            activeData: []
        },
        {
            id: 1,
            title: 'Размер',
            slug: 'size',
            data: [
                {
                    id: 0,
                    attr_id: 1,
                    name: 'XS'
                },
                {
                    id: 1,
                    attr_id: 1,
                    name: 'S'
                },
                {
                    id: 2,
                    attr_id: 1,
                    name: 'M'
                },
                {
                    id: 3,
                    attr_id: 1,
                    name: 'L'
                },
            ],
            activeData: []
        }
    ]
};

const mutations = {
    setFilterToAttribute(state, data){
        state.list.forEach((attr, index) => {
            if (attr.id === data.attr_id && !state.list[index].activeData.includes(data.filter_id)) {
                state.list[index].activeData.push(data.filter_id);
            }
        });
    },
    setAttrActiveData(state,data) {
        state.list.forEach((attr, index) => {
            if (attr.id === data.attr_id) {
                state.list[index].activeData = data.filters;
            }
        });
    }
};

const getters = {
    getAtrributesByID(state) {
        return ids => state.list.filter(item => {
            return ids.includes(item.id);
        });
    },
    getFiltredProducts(state) {
        return (products) => products.filter(product => {
            let status = [];

            state.list.forEach(filter => {

                if (!filter.activeData.length) {
                    status.push(true);
                } else {
                    let filterStatus = false;

                    product[filter.slug].forEach(item => {
                        if (filter.activeData.find(data => data === item)) {
                            filterStatus = true;
                        }
                    });

                    status.push(filterStatus);
                }

            });

            return status.find(stat => stat === false) === undefined;
        });
    },
    getActiveFilters(state) {
        let filters = [];

        state.list.forEach(attr => {
            attr.activeData.forEach(id => {
                let filter = attr.data.find(item => item.id === id);
                if (filter) {
                    filters.push(filter);
                }
            })
        });

        return filters;
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    getters
}