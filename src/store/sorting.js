const state = {
    list: [
        {id: 0, key: 'popularity', name: 'По популярности'},
        {id: 1, key: 'new', name: 'По новизне'},
        {id: 2, key: 'price_to_top', name: 'По цене: от низкой к высокой'},
        {id: 3, key: 'price_to_low', name: 'По цене: от высокой к низкой'},
    ],
    activeKey: 'new'
};

const mutations = {
    setActiveKey(state, key){
        state.activeKey = key;
    }
};

const getters = {
    getSortProducts(state) {
        return (products) => {
            return products.sort((a, b) => {
                switch (state.activeKey) {
                    case 'popularity':
                        return b.views - a.views;
                    case 'price_to_low':
                        return b.price - a.price;
                    case 'price_to_top':
                        return a.price - b.price;
                    default:
                        return b.dateCreate - a.dateCreate;
                }
            });
        }
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    getters
}