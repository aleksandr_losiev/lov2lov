const imagesDir = '/assets/img/friends/';

const state = {
    list: [
        {id: 0, name: 'Кэти Перри', customClass: '', productID: 1, image: imagesDir + 'friend_1.jpg', instagram: 'https://www.instagram.com'},
        {id: 1, name: 'Тейлор Свифт', customClass: '', productID: 2, image: imagesDir + 'friend_2.jpg', instagram: 'https://www.instagram.com'},
        {id: 2, name: 'Стиви Никс', customClass: '', productID: 3, image: imagesDir + 'friend_3.jpg', instagram: 'https://www.instagram.com'},
        {id: 3, name: 'Леди Гага', customClass: '', productID: 4, image: imagesDir + 'friend_4.jpg', instagram: 'https://www.instagram.com'},
        {id: 4, name: 'Бейонсе', customClass: '', productID: 5, image: imagesDir + 'friend_5.jpg', instagram: 'https://www.instagram.com'},
        {id: 5, name: 'Бритни Спирс', customClass: '', productID: 6, image: imagesDir + 'friend_6.jpg', instagram: 'https://www.instagram.com'},
        {id: 6, name: 'Дженнифер Лопез', customClass: '', productID: 1, image: imagesDir + 'friend_7.jpg', instagram: 'https://www.instagram.com'},
        {id: 7, name: 'Миранда Ламберт', customClass: '', productID: 0, image: imagesDir + 'friend_8.jpg', instagram: 'https://www.instagram.com'},
    ]
};

export default {
    namespaced: true,
    state,
}