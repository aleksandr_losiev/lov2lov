const imagesDir = '/assets/img/types/';

const state = {
    list: [
        {id: 0, key: 'party', name: 'Вечер', customClass: '', image: imagesDir + 'type_1.jpg'},
        {id: 1, key: 'day', name: 'Свидание', customClass: '', image: imagesDir + 'type_3.jpg'},
        {id: 2, key: 'work', name: 'Деловой стиль', customClass: '', image: imagesDir + 'type_5.jpg'},
        {id: 3, key: 'relax', name: 'Отдых', customClass: '', image: imagesDir + 'type_2.jpg'},
        {id: 4, key: 'days', name: 'На каждый день', customClass: '', image: imagesDir + 'type_4.jpg'},
        {id: 5, key: 'base', name: 'Базовый гардероб', customClass: '', image: imagesDir + 'type_6.jpg'},
    ]
};

const getters = {
    getTypesByID(state) {
        return ids => state.list.filter( item => {
            return ids.includes(item.id);
        });
    }
};

export default {
    namespaced: true,
    state,
    getters
}