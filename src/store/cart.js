const state = {
    list: [],
    isOpen: false
};

const mutations = {
    setIsOpen(state, status){
        state.isOpen = status;
    },

    setCount(state, data){
        state.list.forEach((item, index) => {
            if (item.id === data.id && data.status === '-') {
                --state.list[index].count
            } else if (item.id === data.id && data.status === '+') {
                ++state.list[index].count
            }
        });
    },
    setProductInCart(state, list){
        state.list = JSON.parse(JSON.stringify(list));
    },
};

const getters = {
    getCountProductsInCart(state) {
        return state.list.length;
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters
}