const imagesDir = '/assets/img/products/';

const state = {
    list: []
};

const mutations = {
    setList(state, list){
        state.list = list;
    }
};

const getters = {
    getProductByID(state) {
        return id => {
            return state.list.find(item => item.id === id);
        }
    },
    getProductsByID(state) {
        return ids => {
            return state.list.filter(product => ids.includes(product.id));
        }
    },
    getProductsByCategory(state) {
        return catID => state.list.filter(item => {
            return item.category.includes(catID);
        });
    },
    getProductsByCategorySlice(state) {
        return (catID, count = 10) => {
            return state.list.filter(item => {
                return item.category.includes(catID);
            }).slice(0, count);
        }
    }
};

const actions = {
    setRandomProducts(context) {
        let data = [];

        let brands = ['ACME', 'Adidas', 'Angara', 'Bashian', 'Burberry', 'Bling Jewelry', 'Calvin Klein', 'Chanel', 'C&K', 'Dior', 'D&G', 'Dsquared2', 'Escada', 'Florsheim', 'Gucci', 'H&M', 'Hurley', 'Varvatos', 'Lark & Ro', 'Moschino'],
            title = ['Toola', 'Loola', 'Aster', 'Lavender', 'Orchid', 'Foxglove', 'Iris', 'Snowdrop', 'Riverside', 'Hilltop'],
            description = [
                'Весеннее, осеннее, летнее и даже зимнее. Кажется, соблюдая режимы стирки, вы можете стать хорошими друзьями.',
                ' Прекрасно смотрится как в городских луках, так и в деловых аутфитах, в сочетании с утонченными жакетами и каблуками. Модель из коллекции Весна – Лето.',
                'Оригинальная кроп-блуза из хлопка со спущенными плечами и вырезом «лодочка». Вырез и рукав по краям дополнены эластичной резинкой, которая фиксирует модель на плечах.',
                'Замшевое платье фасона беби долл. На рукавах широкие манжеты. Круглая горловина зона декольте декорирована завязками',
                "Замшевое платье фасона беби долл. На рукавах широкие манжеты. Круглая горловина зона декольте декорирована завязками",
                'Повседневное платье. Его фасон понравится любителям классики. Модель отличается такими элементами: воротник состоит из двух частей: впереди стойка переходит в отложной.',
                'Платье отрезное по талии со складками защипами, в районе груди выточки, сзади вшита потайная молния',
                'Короткое велюровое платье без бретелей. Модель облегающего кроя отлично подчеркнет вашу фигуру.',
                'Есть пришивной чокер в цвет изделия. Сзади потайная молния. В таком платье восхищенные взгляды окружающих вам обеспечены'
            ],
            sizes = ['XS', 'S', 'M', 'L'];


        for( let i = 0; i < 400; i++){

            let imageRandomUrl = imagesDir + 'or_prod_' + getRandomInt(1, 10) +'.jpg',
                galleryCount = getRandomInt(1, 4),
                colorsCount = getRandomInt(1, 4),
                sizeCount = getRandomInt(1, 2),
                price = getRandomInt(7, 60) * 100,
                gallery = [],
                size = [],
                colors = [];

            for (let l = 0; l < galleryCount; l++) {
                gallery.push(imagesDir + 'or_prod_' + getRandomInt(1, 10) +'.jpg');
            }

            for (let l = 0; l < colorsCount; l++) {
                let id = getRandomInt(0, 10);
                if (!colors.includes(id)) {
                    colors.push(id);
                }
            }

            for (let l = 0; l < sizeCount; l++) {
                let id = getRandomInt(0, 1);
                if (!size.includes(id)) {
                    size.push(id);
                }
            }

            data.push({
                id: i,
                title: title[Math.floor(Math.random()*title.length)] + ' ' + brands[Math.floor(Math.random()*brands.length)],
                category: [getRandomInt(0, 10)],
                price: price - getRandomInt(1, 5) * 100,
                oldPrice: price,
                views: getRandomInt(20, 999),
                size: size,
                dateCreate: getRandomInt(1, 9999),
                attr: [
                    {id: 0, name: 'Вискоза', count: '70', unit: '%'},
                    {id: 0, name: 'Хлопок', count: '30', unit: '%'},
                    {id: 0, name: 'Длина', count: '102', unit: 'см'}
                ],
                meta: {
                    modelHeight: getRandomInt(155, 185),
                    modelSize: sizes[Math.floor(Math.random()*sizes.length)]
                },
                description: description[Math.floor(Math.random()*description.length)],
                images: {
                    thumbnail: imageRandomUrl,
                    main: imageRandomUrl,
                    gallery: gallery
                },
                colors: colors
            })
        }

        context.commit('setList', data);
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}