export default {
    methods: {
        getProductDataByID(id) {
            let product = this.$store.getters['products/getProductByID'](id);
            product.categoryData = this.$store.getters['categories/getCategoriesByID'](product.category);

            return product;
        },
        setToFavorite(product_id) {
            let fovoriteList = this.$store.state.main.favorite.productsID;
            let indexFavoriteProduct = fovoriteList.findIndex(id => id === product_id);

            if (indexFavoriteProduct === -1) {
                fovoriteList.push(product_id);
            } else {
                fovoriteList.splice(indexFavoriteProduct, 1);
            }

            this.$store.commit('main/setProductInFavorite', fovoriteList);
        },
        isProductInFavorite(product_id) {
            return Boolean(this.$store.state.main.favorite.productsID.find(id => id === product_id));
        }
    }
}