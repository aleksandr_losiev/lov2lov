export default {
    methods: {
        /**
         *  Close opened navigation or sidebars
         *  @param navigation_types array
         */
        closeNavigation(navigation_types) {
            navigation_types.forEach(type => {
                switch (type) {
                    case 'history':
                        this.$store.commit('main/setHistoryStatus', false);
                        break;
                    case 'favorite':
                        this.$store.commit('main/setFavoriteStatus', false);
                        break;
                    case 'cart':
                        this.$store.commit('cart/setIsOpen', false);
                        break;
                    case 'navigation':
                        this.$store.commit('main/setNavigationStatus', false);
                        break;
                    case 'menu':
                        this.$store.commit('main/setMenuStatus', false);
                        break;
                    case 'sorting':
                        this.$store.commit('main/setIsOpenSorting', false);
                        break;
                    case 'filter':
                        this.$store.commit('main/setIsOpenFilter', false);
                        break;
                    case 'all':
                        this.$store.commit('main/setHistoryStatus', false);
                        this.$store.commit('main/setFavoriteStatus', false);
                        this.$store.commit('cart/setIsOpen', false);
                        this.$store.commit('main/setNavigationStatus', false);
                        break;
                }
            });
            this.$store.commit('main/setCloseModal', false);
        },

        /**
         * Check is product has sale category
         * @param product_id integer
         * @param sale_category_ID integer
         * @return boolean
         */
        isProductSale(product_id, sale_category_ID = 10) {
            let productData = this.$store.getters['products/getProductByID'](product_id);

            return Boolean(productData.category.find(cat => cat === sale_category_ID));
        }
    }
}
