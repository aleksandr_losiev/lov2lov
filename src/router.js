import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home'
import Category from './views/Category'
import Product from './views/Product'
import Type from './views/Type'
import About from './views/About'
import Regulations from './views/Regulations'
import Delivery from './views/Delivery'
import Checkout from './views/Checkout'
import Contacts from './views/Contacts'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/category/:name',
            name: 'category',
            component: Category
        },
        {
            path: '/product/:name',
            name: 'product',
            component: Product
        },
        {
            path: '/types/:name',
            name: 'type',
            component: Type
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: Checkout
        },
        {
            path: '/regulations',
            name: 'regulations',
            component: Regulations
        },
        {
            path: '/delivery',
            name: 'delivery',
            component: Delivery
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: Contacts
        },
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})
